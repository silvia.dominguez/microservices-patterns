This repo is about Observability (Distributed Tracing)
    

## Introduction and History
There are 3 ways of collecting data for observing microservices: *Logging*, *metrics* and *distributed tracing*.
We'll focus on Distributed Tracing.
Distributed tracing is a diagnostic technique that reveals how a set of services coordinate to handle individual user request.

A single trace shows the activity for an indivudual transaction or request as it propagates trough an application.In other words, distributed tracing is about 
to understanding the path of data as it propagates trough the components of our application.
While logs (see [https://microservices.io/patterns/observability/application-logging.html] can record important checkpoints when servicing a request, a trace
connects all these checkpoints into a complete route that explains how that request was handled across all services from start to finish.

In this type of architectures, a request can trigger several internal calls between microservices, so it's important to associate a unique request identifier, so 
that it can propagate and be able to track the request in a centralized way; Spring offers us tools that facilitate this work: Spring cloud sleuth and zipkin.

Distributed tracing, also called distributed request tracing, is a method used to profile and monitor applications, especially those built using a 
microservices architecture. Distributed tracing helps pinpoint where failures occur and what causes poor performance.


run jaeger

docker run -d -p6831:6831/udp -p16686:16686 jaegertracing/all-in-one:latest